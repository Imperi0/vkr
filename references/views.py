from django.urls import reverse
from material import LayoutMixin, Layout, Row, Fieldset
from material.frontend.views import ModelViewSet, CreateModelView, UpdateModelView
from django.utils.translation import gettext_lazy as _

from . import models


class CommonCreateModelView(CreateModelView):
    """
    Класс создан для простого редиректа в список нужной модели после
    успешного создания объекта.
    Методы этому классу скорее всего будут не нужны.
    """

    def get_success_url(self):
        opts = self.viewset.model._meta
        return reverse('{}:{}_change'.format(opts.app_label, opts.model_name), args=[self.object.pk])


class CommonUpdateModelView(UpdateModelView):
    """
    Класс создан для простого редиректа в список нужной модели после
    успешного изменения объекта.
    Методы этому классу скорее всего будут не нужны.
    """

    def get_success_url(self):
        opts = self.viewset.model._meta
        return reverse('{}:{}_change'.format(opts.app_label, opts.model_name), args=[self.object.pk])


class OrganizationViewSet(LayoutMixin, ModelViewSet):
    model = models.Organization
    create_view_class = CommonCreateModelView
    update_view_class = CommonUpdateModelView
    list_display = ('name', 'is_sponsor')
    layout = Layout(
        'name',
        'is_sponsor',
        Fieldset(
            _('Company details'),
            Row('inn', 'kpp'))
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return request.user.is_authenticated() and request.user.is_superuser


class TenderViewSet(LayoutMixin, ModelViewSet):
    model = models.Tender
    create_view_class = CommonCreateModelView
    update_view_class = CommonUpdateModelView
    list_display = ('name', 'sponsor', 'date_start', 'date_end', 'amount')
    layout = Layout(
        'name',
        'description',
        Fieldset(
            _('Tender details'),
            'sponsor', 'amount',
            Row('date_start', 'date_end'))
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return request.user.is_authenticated() and request.user.is_superuser
