from django.db import models
from django.utils.translation import gettext as _


# Create your models here.

class Organization(models.Model):
    name = models.CharField(_('Name'), max_length=300)
    inn = models.CharField(_('INN'), max_length=12, blank=True)
    kpp = models.CharField(_('KPP'), max_length=9, blank=True)
    is_sponsor = models.BooleanField(default=False, verbose_name=_('Is sponsor'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')


class Tender(models.Model):
    name = models.CharField(_('Name'), max_length=300)
    description = models.TextField(_('Description'))
    amount = models.FloatField(_('Amount'))
    date_start = models.DateField(_('Start date'))
    date_end = models.DateField(_('End date'))
    sponsor = models.ForeignKey(Organization, verbose_name=_('Sponsor'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Tender')
        verbose_name_plural = _('Tenders')
