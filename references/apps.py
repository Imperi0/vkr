from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _
from material.frontend.apps import ModuleMixin


class ReferencesConfig(ModuleMixin, AppConfig):
    name = 'references'
    verbose_name = _('References')
