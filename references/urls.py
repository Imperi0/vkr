from django.conf.urls import url, include
from django.views import generic

from . import views

urlpatterns = [
    url('^$', generic.RedirectView.as_view(url='./organizations/'), name="index"),
    url('^organizations/', include(views.OrganizationViewSet().urls)),
    url('^tenders/', include(views.TenderViewSet().urls))
]
