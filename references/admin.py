from django.contrib import admin

from .models import Tender, Organization

admin.site.register(Organization)
admin.site.register(Tender)
