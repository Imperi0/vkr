from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.db.models import Q
from django.utils.translation import gettext as _
from . import models


class PreferenceForm(forms.ModelForm):
    class Meta:
        model = models.OrganizationPreference
        fields = ('prefer',)

    def __init__(self, *args, **kwargs):
        super(PreferenceForm, self).__init__(*args, **kwargs)
        self.fields['prefer'].queryset = models.Organization.objects.filter(is_sponsor=False)
        self.fields['prefer'].empty_label = None
        if hasattr(self, 'initial') and self.initial['prefer'] is not None:
            value = self.initial['prefer'].pk
            self.fields['prefer'].widget.attrs['value'] = value
        if hasattr(self, 'cleaned_data') and self.cleaned_data['prefer'] is not None:
            value = self.cleaned_data['prefer'].pk
            self.fields['prefer'].widget.attrs['value'] = value

    def save(self, **kwargs):
        prefer = None
        if hasattr(self, 'cleaned_data') and self.cleaned_data['prefer'] is not None:
            prefer = self.cleaned_data['prefer']
        if hasattr(self, 'initial') and self.initial['prefer'] is not None and self.cleaned_data['prefer'] is None:
            prefer = self.initial['prefer']
        self.instance.prefer = prefer
        self.instance.save()
        return self.instance


class SponsorPreferenceForm(forms.ModelForm):
    class Meta:
        model = models.SponsorPreference
        fields = ('prefer',)

    def __init__(self, *args, **kwargs):
        super(SponsorPreferenceForm, self).__init__(*args, **kwargs)
        self.fields['prefer'].empty_label = None
        if hasattr(self, 'initial') and self.initial['prefer'] is not None:
            value = self.initial['prefer'].pk
            self.fields['prefer'].widget.attrs['value'] = value
        if hasattr(self, 'cleaned_data') and self.cleaned_data['prefer'] is not None:
            value = self.cleaned_data['prefer'].pk
            self.fields['prefer'].widget.attrs['value'] = value

    def save(self, **kwargs):
        prefer = None
        if hasattr(self, 'cleaned_data') and self.cleaned_data['prefer'] is not None:
            prefer = self.cleaned_data['prefer']
        if hasattr(self, 'initial') and self.initial['prefer'] is not None and self.cleaned_data['prefer'] is None:
            prefer = self.initial['prefer']
        self.instance.prefer = prefer
        self.instance.save()
        return self.instance


class PreferredByOrganizationForm(forms.ModelForm):
    class Meta:
        model = models.PreferredByOrganization
        fields = ('preferred', 'rank')


class PreferredBySponsorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PreferredBySponsorForm, self).__init__(*args, **kwargs)
        self.fields['preferred'].queryset = models.Organization.objects.filter(is_sponsor=False)

    class Meta:
        model = models.PreferredBySponsor
        fields = ('preferred', 'rank')


class BasePreferredFormSet(forms.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        preferences = []
        ranks = []
        for form in self.forms:
            if not form.cleaned_data:
                raise forms.ValidationError(_('All forms must be filled in'))
            rank = form.cleaned_data['rank']
            preferred = form.cleaned_data['preferred']
            if not form.cleaned_data['DELETE']:
                if preferred in preferences:
                    raise forms.ValidationError(_('Preferred tenders should be unique'))
                if rank in ranks:
                    raise forms.ValidationError(_('Rank per tender should be unique'))
                preferences.append(preferred)
                ranks.append(rank)
        if len(preferences) == 0:
            raise forms.ValidationError(_('There must be at least one preferred'))


class StablePairForm(forms.Form):
    methods = (('man', '"Мужской" алгоритм'), ('woman', '"Женский" алгоритм'), ('fair', 'Справедливое решение'))
    method = forms.ChoiceField(choices=methods, widget=forms.RadioSelect(choices=methods),
                               initial='fair', label=_('Method'))


class AdminUserAddForm(UserCreationForm):
    class Meta:
        model = models.SystemUser
        fields = '__all__'

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            models.SystemUser._default_manager.get(username=username)
        except models.SystemUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class AdminUserChangeForm(UserChangeForm):
    class Meta:
        model = models.SystemUser
        fields = '__all__'
