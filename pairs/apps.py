from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _
from material.frontend.apps import ModuleMixin


class PairsConfig(ModuleMixin, AppConfig):
    name = 'pairs'
    icon = '<i class="material-icons">compare_arrows</i>'

    verbose_name = _('Matching')
