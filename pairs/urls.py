from django.conf.urls import url, include
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic import RedirectView

from . import views


class LoginRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        user = self.request.user
        if user.is_superuser:
            redir_url = './organization_preferences/'
        else:
            if user.organization.is_sponsor:
                redir_url = './sponsor_preferences/'
            else:
                redir_url = './organization_preferences/'
        return redir_url


urlpatterns = [
    url('^$', LoginRedirectView.as_view(), name="index"),
    url('^organization_preferences/', include(views.PreferenceViewSet().urls)),
    url(r'^sponsor_preferences/', include(views.SponsorPreferenceViewSet().urls)),
    url('^stable_pairs/', views.get_stable_pair, name='stable_pairs')
]
