from django.core.management import BaseCommand
from pairs.models import SystemUser, Preference, PreferredUsers


class Command(BaseCommand):
    def handle(self, *args, **options):
        students = SystemUser.objects.filter(groups__name='Student').order_by('pk')
        lectures = SystemUser.objects.filter(groups__name='Expert').order_by('pk')
        tmp_a = [i for i in range(len(students))]
        tmp_b = [*[i for i in range(1, len(students))], 0]
        # build students preferences
        for i, val in enumerate(students):
            p = Preference.objects.create(user=val)
            if i:
                tmp_a = [*tmp_a[1:], *tmp_a[:1]]
            for j, item in enumerate(tmp_a):
                PreferredUsers.objects.create(preference=p, preference_user=lectures[item], rank=j + 1)
        # build lectures preferences
        for i, val in enumerate(lectures):
            p = Preference.objects.create(user=val)
            if i:
                tmp_b = [*tmp_b[1:], *tmp_b[:1]]
            for j, item in enumerate(tmp_b):
                PreferredUsers.objects.create(preference=p, preference_user=students[item], rank=j + 1)
