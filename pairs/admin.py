from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext as _

from .forms import AdminUserAddForm, AdminUserChangeForm
from .models import OrganizationPreference, SystemUser, SponsorPreference


class UserAdmin(BaseUserAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            ('last_name', 'first_name', 'middle_name',),
            'email',
            'position',
            'gender',
            'birth_day',
            'organization'
        )}),
        (_('Permissions'), {'fields': (('is_staff', 'is_superuser', 'is_active',), 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(SystemUser, UserAdmin)
admin.site.register(OrganizationPreference)
admin.site.register(SponsorPreference)
