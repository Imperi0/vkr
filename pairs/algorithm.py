class Pair:
    def __init__(self, A: dict, B: dict, key=None):
        self.A = A
        self.B = B
        if key:
            self.A, self.B = self.B, self.A

    def gen_pair(self):
        rA = {}
        rB = {b: None for b in self.B}
        while len(rA) < len(self.A):
            for a in self.A:
                b = self.A[a][0]
                c = rB[b]
                if a in self.B[b]:
                    if not c:
                        rA[a] = b
                        rB[b] = a
                    elif c and self.B[b].index(a) < self.B[b].index(c):
                        rA[a] = b
                        rB[b] = a
                        rA.pop(c)
                        self.A[c].remove(b)
                    elif self.B[b].index(a) != self.B[b].index(c):
                        self.A[a].remove(b)
        return {k: rA[k] for k in sorted(rA)}

    def swap(self):
        self.A, self.B = self.B, self.A

    def remove(self, a, b):
        self.A.pop(a)
        for item_a in self.A:
            self.A[item_a].remove(b)
        self.B.pop(b)
        for item_b in self.B:
            self.B[item_b].remove(a)

    def fair(self):
        res = {b: None for b in self.B}
        for a in self.A:
            for b in self.B:
                if self.A[a].index(b) == self.B[b].index(a):
                    res[b] = a
        return res

    def fair_exist(self) -> bool:
        return all([item for item in self.fair()])
