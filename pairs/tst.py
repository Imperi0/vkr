import ipdb

from django.contrib.auth.models import User, Group
from pairs.models import Preference, PreferredUsers

ipdb.set_trace()
students = User.objects.filter(groups=Group.objects.filter(name='Student').first()).order_by('username').values_list(
    'pk', flat=True)
experts = User.objects.filter(groups=Group.objects.filter(name='Expert').first()).order_by('username').values_list('pk',
                                                                                                                   flat=True)
if experts.count() > students.count():
    experts = experts[:students.count() - 1]
elif students.count() > experts.count():
    students = students[:experts.count() - 1]
preferences = []
for i, student in enumerate(students):
    if i == 0:
        preference = []
        u = Preference.objects.create(user_id=student)
        for j, expert in enumerate(experts):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=expert, rank=j + 1)]
    else:
        preference = []
        u = Preference.objects.create(user_id=student)
        for j, expert in enumerate(experts[i:]):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=expert, rank=j + 1)]
        n = len(preference)
        for j, expert in enumerate(experts[:i]):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=expert, rank=j + 1 + n)]
    preferences += preference

for i, expert in enumerate(experts):
    if i == 0:
        preference = []
        u = Preference.objects.create(user_id=expert)
        for j, student in enumerate(students):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=student, rank=j + 1)]
    else:
        preference = []
        u = Preference.objects.create(user_id=expert)
        for j, student in enumerate(students[i:]):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=student, rank=j + 1)]
        n = len(preference)
        for j, student in enumerate(students[:i]):
            preference += [PreferredUsers.objects.create(preference=u, preference_user_id=student, rank=j + 1 + n)]
    preferences += preference
