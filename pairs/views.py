import random

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import render
from django.utils.translation import gettext as _
from material import Layout, Row, Fieldset
from material.frontend.views import ModelViewSet, CreateModelView, UpdateModelView, ListModelView

from references.models import Organization, Tender
from .algorithm import Pair
from .forms import PreferenceForm, StablePairForm, BasePreferredFormSet, PreferredByOrganizationForm, \
    SponsorPreferenceForm, PreferredBySponsorForm
from .models import SponsorPreference, OrganizationPreference, SystemUser, \
    PreferredByOrganization, PreferredBySponsor


# get_preferred_user_form, BasePreferredUserFormSet


def get_stable_pair(request):
    if request.method == 'POST':
        form = StablePairForm(request.POST)
        if form.is_valid():
            tenders = Tender.objects.all()
            organizations = Organization.objects.filter(is_sponsor=False)
            A, B = {}, {}
            for tender in tenders:
                key = str(tender)
                A[key] = [str(i.preferred) for i in tender.sponsorpreference.get_preferences()]
            for organization in organizations:
                key = str(organization)
                B[key] = [str(i.preferred) for i in organization.organizationpreference.get_preferences()]
            method = form.cleaned_data['method']
            if method == 'man':
                res = Pair(A, B).gen_pair()
            elif method == 'woman':
                res = Pair(B, A).gen_pair()
            elif method == 'fair':
                a = Pair(B, A)
                if a.fair_exist():
                    res = a.fair()
                else:
                    random.seed()
                    rnd = random.randint(0, 1)
                    res = Pair(A, B) if rnd else Pair(B, A)
                    res = res.gen_pair()
            method = ' '.join([method, 'algorithm'])
            return render(request, 'stable/detail.html', {'A': A, 'B': B, 'res': res, 'method': _(method)})
    else:
        form = StablePairForm()

    return render(request, 'stable/form.html', {'form': form})


create_update_layout = Layout(Row(Fieldset(None, 'prefer')),
                              Row(Fieldset(_('Preferreds'))))


class PreferenceCreateView(LoginRequiredMixin, CreateModelView):
    form_class = PreferenceForm
    layout = create_update_layout

    def get_form(self, form_class=None):
        form = super(PreferenceCreateView, self).get_form(form_class)
        is_superuser = self.request.user.is_superuser
        attrs = form.fields['prefer'].widget.attrs
        # если пользователь -- администратор, то менять prefer нельзя
        attrs['readonly'], attrs['disabled'] = not is_superuser, not is_superuser
        return form

    def get_initial(self):
        user = SystemUser.objects.get(username=self.request.user.username)
        if user.organization:
            organization = user.organization
        else:
            organization = None
        return {'prefer': organization}

    def get_context_data(self, **kwargs):
        context = super(PreferenceCreateView, self).get_context_data(**kwargs)
        user = self.request.user
        query = Tender.objects.all()
        formset_cls = forms.inlineformset_factory(OrganizationPreference, PreferredByOrganization,
                                                  form=PreferredByOrganizationForm,
                                                  formset=BasePreferredFormSet,
                                                  extra=0, max_num=query.count())
        if self.request.POST:
            context['preferences'] = formset_cls(self.request.POST)
        else:
            context['preferences'] = formset_cls()
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        preferences = context['preferences']
        with transaction.atomic():
            self.object = form.save()
            if preferences.is_valid():
                preferences.instance = self.object
                preferences.save()
            else:
                print(preferences.errors)
        return super(PreferenceCreateView, self).form_valid(form)


class PreferenceUpdateView(UpdateModelView):
    form_class = PreferenceForm
    layout = create_update_layout

    def get_context_data(self, **kwargs):
        context = super(PreferenceUpdateView, self).get_context_data(**kwargs)
        user = self.request.user
        query = Tender.objects.all()
        formset_cls = forms.inlineformset_factory(OrganizationPreference, PreferredByOrganization,
                                                  form=PreferredByOrganizationForm,
                                                  formset=BasePreferredFormSet,
                                                  extra=0, max_num=query.count())
        if self.request.POST:
            context['preferences'] = formset_cls(self.request.POST,
                                                 instance=self.object)
        else:
            context['preferences'] = formset_cls(instance=self.object)
        context['preferences_readonly'] = False
        return context

    def get_form(self, form_class=None):
        form = super(PreferenceUpdateView, self).get_form(form_class)
        is_superuser = self.request.user.is_superuser
        attrs = form.fields['prefer'].widget.attrs
        # если пользователь -- администратор, то менять prefer нельзя
        attrs['readonly'], attrs['disabled'] = not is_superuser, not is_superuser
        return form

    def get_initial(self):
        return {'prefer': self.object.prefer}

    def form_valid(self, form):
        context = self.get_context_data()
        preferences = context['preferences']
        with transaction.atomic():
            self.object = form.save()
            if preferences.is_valid():
                preferences.instance = self.object
                preferences.save()
            else:
                print(preferences.errors)
        return super(PreferenceUpdateView, self).form_valid(form)


class PreferenceDetailView(PreferenceUpdateView):
    template_name_suffix = '_detail'

    def get_form(self, **kwargs):
        form = super(PreferenceDetailView, self).get_form()
        for field in form.fields:
            form.fields[field].widget.attrs['readonly'] = 'readonly'
            form.fields[field].widget.attrs['disabled'] = 'disabled'
        return form

    def get_context_data(self, **kwargs):
        context = super(PreferenceDetailView, self).get_context_data(**kwargs)
        context['preferences_readonly'] = True
        return context


class PreferenceListView(ListModelView):
    ordering = ('prefer',)
    datatable_config = {
        "columns": [
            {"data": "prefer"},
            {"data": "preferred",
             "title": _("Preferred"),
             "orderable": False
             },
            {"data": "preferred_rank",
             "title": _("Rank"),
             "orderable": False
             },
        ],
        "oLanguage": {
            "sEmptyTable": str(_("No Preferences")),
            "oPaginate": {
                "sNext": '&rang;',
                "sPrevious": '&lang;'
            }
        }
    }

    def get_queryset(self, **kwargs):
        qs = super(PreferenceListView, self).get_queryset(**kwargs)
        user = self.request.user
        if not user.is_superuser:
            if not user.organization.is_sponsor:
                qs = qs.filter(prefer=user.organization)
            else:
                qs = qs.filter(prefer__sponsor=user.organization)
        return qs


class PreferenceViewSet(ModelViewSet):
    create_view_class = PreferenceCreateView
    update_view_class = PreferenceUpdateView
    detail_view_class = PreferenceDetailView
    list_view_class = PreferenceListView
    list_display = ('prefer', 'preferred', 'preferred_rank')
    model = OrganizationPreference

    def has_add_permission(self, request):
        if not request.user.is_authenticated():
            return False
        if not request.user.is_superuser:
            query = OrganizationPreference.objects.filter(prefer=request.user.organization)
            return not query.exists()
        else:
            return OrganizationPreference.objects.count() < Organization.objects.filter(is_sponsor=False).count()

    def has_view_permission(self, request, obj=None):
        if not request.user.is_authenticated:
            return False
        elif request.user.is_superuser:
            return True
        else:
            return not request.user.organization.is_sponsor


class SponsorPreferenceCreateView(CreateModelView):
    form_class = SponsorPreferenceForm
    layout = create_update_layout

    def get_form(self, form_class=None):
        form = super(SponsorPreferenceCreateView, self).get_form(form_class)
        is_superuser = self.request.user.is_superuser
        attrs = form.fields['prefer'].widget.attrs
        # если пользователь -- администратор, то менять prefer нельзя
        attrs['readonly'], attrs['disabled'] = not is_superuser, not is_superuser
        return form

    def get_initial(self):
        user = SystemUser.objects.get(username=self.request.user.username)
        if user.organization:
            organization = user.organization
        else:
            organization = None
        if organization:
            tender = Tender.objects.filter(sponsor=organization).first()
        else:
            tender = None
        return {'prefer': tender}

    def get_context_data(self, **kwargs):
        context = super(SponsorPreferenceCreateView, self).get_context_data(**kwargs)
        user = self.request.user
        query = Organization.objects.filter(is_sponsor=False)
        formset_cls = forms.inlineformset_factory(SponsorPreference, PreferredBySponsor,
                                                  form=PreferredBySponsorForm,
                                                  formset=BasePreferredFormSet,
                                                  extra=0, max_num=query.count())
        if self.request.POST:
            context['preferences'] = formset_cls(self.request.POST)
        else:
            context['preferences'] = formset_cls()
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        preferences = context['preferences']
        with transaction.atomic():
            self.object = form.save()
            if preferences.is_valid():
                preferences.instance = self.object
                preferences.save()
            else:
                print(preferences.errors)
        return super(SponsorPreferenceCreateView, self).form_valid(form)


class SponsorPreferenceUpdateView(UpdateModelView):
    form_class = SponsorPreferenceForm
    layout = create_update_layout

    def get_context_data(self, **kwargs):
        context = super(SponsorPreferenceUpdateView, self).get_context_data(**kwargs)
        user = self.request.user
        query = Organization.objects.filter(is_sponsor=False)
        formset_cls = forms.inlineformset_factory(SponsorPreference, PreferredBySponsor,
                                                  form=PreferredBySponsorForm,
                                                  formset=BasePreferredFormSet,
                                                  extra=0, max_num=query.count())
        if self.request.POST:
            context['preferences'] = formset_cls(self.request.POST,
                                                 instance=self.object)
        else:
            context['preferences'] = formset_cls(instance=self.object)
        context['preferences_readonly'] = False
        return context

    def get_form(self, form_class=None):
        form = super(SponsorPreferenceUpdateView, self).get_form(form_class)
        is_superuser = self.request.user.is_superuser
        attrs = form.fields['prefer'].widget.attrs
        # если пользователь -- администратор, то менять prefer нельзя
        attrs['readonly'], attrs['disabled'] = not is_superuser, not is_superuser
        return form

    def get_initial(self):
        return {'prefer': self.object.prefer}

    def form_valid(self, form):
        context = self.get_context_data()
        preferences = context['preferences']
        with transaction.atomic():
            self.object = form.save()
            if preferences.is_valid():
                preferences.instance = self.object
                preferences.save()
            else:
                print(preferences.errors)
        return super(SponsorPreferenceUpdateView, self).form_valid(form)


class SponsorPreferenceDetailView(SponsorPreferenceUpdateView):
    template_name_suffix = '_detail'

    def get_form(self, **kwargs):
        form = super(SponsorPreferenceDetailView, self).get_form()
        for field in form.fields:
            form.fields[field].widget.attrs['readonly'] = 'readonly'
            form.fields[field].widget.attrs['disabled'] = 'disabled'
        return form

    def get_context_data(self, **kwargs):
        context = super(SponsorPreferenceDetailView, self).get_context_data(**kwargs)
        context['preferences_readonly'] = True
        return context


class SponsorPreferenceViewSet(ModelViewSet):
    create_view_class = SponsorPreferenceCreateView
    update_view_class = SponsorPreferenceUpdateView
    detail_view_class = SponsorPreferenceDetailView
    list_view_class = PreferenceListView
    list_display = ('prefer', 'preferred', 'preferred_rank')
    model = SponsorPreference

    def has_add_permission(self, request):
        if not request.user.is_authenticated():
            return False
        if not request.user.is_superuser:
            query = SponsorPreference.objects.filter(prefer__sponsor=request.user.organization)
            return not query.exists()
        else:
            return SponsorPreference.objects.count() < Organization.objects.filter(is_sponsor=True).count()

    def has_view_permission(self, request, obj=None):
        if not request.user.is_authenticated:
            return False
        elif request.user.is_superuser:
            return True
        else:
            return request.user.organization.is_sponsor
