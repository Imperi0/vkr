from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext as _


# Create your models here.
from references.models import Organization, Tender


class SystemUser(AbstractUser):
    middle_name = models.CharField(_('middle name'),
                                   max_length=50,
                                   blank=True)
    gender = models.CharField(_('gender'),
                              max_length=10,
                              choices=(('men', _('men')), ('women', _('women'))),
                              blank=True)
    position = models.CharField(_('position'),
                                max_length=50,
                                blank=True)
    birth_day = models.DateField(_('birthday'),
                                 blank=True, null=True)
    organization = models.ForeignKey(Organization, verbose_name=_('Organization'),
                                     null=True, blank=True)

    def get_full_name(self):
        name = [i for i in [self.last_name, self.first_name, self.middle_name] if i]
        name = [name[0] if name else self.username] + [i for i in name[1:]]
        return ' '.join(name).strip()

    def __str__(self):
        return self.get_full_name()


class OrganizationPreference(models.Model):
    prefer = models.OneToOneField(Organization, verbose_name=_('Organization that prefers'))

    def __str__(self):
        return '{}'.format(self.prefer)

    def get_preferences(self):
        return self.preferredbyorganization_set.all().order_by('rank')

    @property
    def preferred(self):
        return '<br>'.join([str(i.preferred) for i in self.get_preferences()])

    @property
    def preferred_rank(self):
        return '<br>'.join(str(i.rank) for i in self.get_preferences())

    class Meta:
        verbose_name = _('Preference Of Organization')
        verbose_name_plural = _('Preferences of Organizations')


class PreferredByOrganization(models.Model):
    preference = models.ForeignKey(OrganizationPreference)
    preferred = models.ForeignKey(Tender, verbose_name=_('preferred'))
    rank = models.IntegerField(verbose_name=_('rank'))

    class Meta:
        unique_together = ['preference', 'preferred', 'rank']
        verbose_name = _('Preferred')
        verbose_name_plural = _('Preferreds')


class SponsorPreference(models.Model):
    prefer = models.OneToOneField(Tender, verbose_name=_('Sponsor that prefers'))

    def get_preferences(self):
        return self.preferredbysponsor_set.all().order_by('rank')

    @property
    def preferred(self):
        return '<br>'.join([str(i.preferred) for i in self.get_preferences()])

    @property
    def preferred_rank(self):
        return '<br>'.join(str(i.rank) for i in self.get_preferences())

    def __str__(self):
        return str(self.prefer)

    class Meta:
        verbose_name = _('Preference of Sponsor')
        verbose_name_plural = _('Preferences of Sponsors')


class PreferredBySponsor(models.Model):
    preference = models.ForeignKey(SponsorPreference)
    preferred = models.ForeignKey(Organization, verbose_name=_('preferred'))
    rank = models.IntegerField(verbose_name=_('rank'))

    class Meta:
        unique_together = ['preference', 'preferred', 'rank']
        verbose_name = _('Preferred')
        verbose_name_plural = _('Preferreds')
